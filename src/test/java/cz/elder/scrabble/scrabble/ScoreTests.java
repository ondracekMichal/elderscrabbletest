package cz.elder.scrabble.scrabble;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;


import cz.elder.scrabble.scrabble.domain.Member;
import cz.elder.scrabble.scrabble.domain.Score;
import cz.elder.scrabble.scrabble.service.GameService;
import cz.elder.scrabble.scrabble.service.MemberService;
import cz.elder.scrabble.scrabble.service.ScoreboardService;
import cz.elder.scrabble.scrabble.service.dto.GameScoreDTO;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
@SpringBootTest
class ScoreTests {

	@Autowired
	private MemberService memberService;

	@Autowired
	private GameService gameService;

	@Autowired
	ScoreboardService scoreboardService;

	private final int LOWEST_SCORE = 0;
	private final int HIGHEST_SCORE = 999;

	private Member winner;
	private Member loser;


	@BeforeEach
	void init() {
		winner = createMember();
		loser = createMember();

		GameScoreDTO gameScore = new GameScoreDTO();
		gameScore.setLoserMemberId(loser.getMemberId());
		gameScore.setLoserScore(LOWEST_SCORE);
		gameScore.setWinnerMemberId(winner.getMemberId());
		gameScore.setWinnerScore(HIGHEST_SCORE);

		gameService.saveNewGame(gameScore);
	}

	@Test
	void highestScoreTest() {
		final Score highestScore = scoreboardService.getHighestScore();
		assertEquals(HIGHEST_SCORE, highestScore.getScore());
	}

	@Test
	void lowestScoreTest() {
		final Score lowestScore = scoreboardService.getLowestScore();
		assertEquals(LOWEST_SCORE, lowestScore.getScore());
	}

	@Test
	void highestMemberScoreTest() {
		final Score highestScoreWinner = scoreboardService.getHighestScore(winner.getMemberId());
		final Score highestScoreLoser = scoreboardService.getHighestScore(loser.getMemberId());

		assertEquals(HIGHEST_SCORE, highestScoreWinner.getScore());
		assertEquals(LOWEST_SCORE, highestScoreLoser.getScore());
	}

	@Test
	void averageMemberScoreTest() {
		final Double avgScoreWinner = scoreboardService.getAvgScore(winner.getMemberId());
		final Double avgScoreLoser = scoreboardService.getAvgScore(loser.getMemberId());

		assertEquals(Double.valueOf(HIGHEST_SCORE), avgScoreWinner);
		assertEquals(Double.valueOf(LOWEST_SCORE), avgScoreLoser);
	}



	private Member createMember() {
		final Member member = new Member();
		member.setEmail("test@email.cz");
		member.setName("test name");
		member.setPhoneNumber("607897898");
		return memberService.registerNewMember(member);
	}

}
