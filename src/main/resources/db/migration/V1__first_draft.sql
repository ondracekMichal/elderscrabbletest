create table member
(
    member_id uuid primary key,
    name varchar,
    email varchar,
    phone_number varchar,
    registration_date timestamp default now() not null
)
;

create table game
(
    game_id uuid primary key,
    played_at timestamp default now() not null
)
;

create table score
(
    member_id uuid references member(member_id) not null,
    game_id uuid references game(game_id) not null,
    score integer,
    winner boolean not null,
    PRIMARY KEY (member_id, game_id)
)
;


create index score_index
    on score (score)
;
