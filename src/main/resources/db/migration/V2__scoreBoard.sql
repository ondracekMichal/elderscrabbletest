create view scoreboard_view as
    SELECT m.name, AVG(s.score) as "average_score" from score s
    join member m on s.member_id = m.member_id
    group by s.member_id, m.name
    having count(*) > 9
    order by AVG(s.score) DESC
    limit 10

