package cz.elder.scrabble.scrabble.controller;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;

import cz.elder.scrabble.scrabble.domain.Member;
import cz.elder.scrabble.scrabble.service.MemberService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class MemberController implements MemberOperations {

	private MemberService memberService;


	@Override
	public List<Member> getAllMembers() {
		return memberService.getAllMembers();
	}

	@Override
	public Optional<Member> getMemberByEmail(String email) {
		return memberService.getMemberByEmail(email);
	}

	@Override
	public void registerNewMember(@Valid Member member) {
		memberService.registerNewMember(member);
	}

	@Override
	public void updateMember(@Valid Member member) {
		memberService.getMemberByMemberId(member.getMemberId())
				.orElseThrow(() -> new IllegalArgumentException("Updated member does not exist"));

		memberService.updateMember(member);
	}
}
