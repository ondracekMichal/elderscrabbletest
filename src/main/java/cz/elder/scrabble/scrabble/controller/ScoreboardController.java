package cz.elder.scrabble.scrabble.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.web.bind.annotation.RestController;

import cz.elder.scrabble.scrabble.domain.Score;
import cz.elder.scrabble.scrabble.domain.views.ScoreboardView;
import cz.elder.scrabble.scrabble.service.ScoreboardService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class ScoreboardController implements ScoreboardOperations {

	private ScoreboardService scoreboardService;

	@Override
	public List<ScoreboardView> getScoreboard() {
		return scoreboardService.getScoreboard();
	}

	@Override
	public Score getHighestScore() {
		return scoreboardService.getHighestScore();
	}

	@Override
	public Score getLowestScore() {
		return scoreboardService.getLowestScore();
	}

	@Override
	public int getMemberWins(UUID memberId) {
		return scoreboardService.getMemberWins(memberId);
	}

	@Override
	public int getMemberLoss(UUID memberId) {
		return scoreboardService.getMemberLoss(memberId);
	}

	@Override
	public Double getAvgScore(UUID memberId) {
		return scoreboardService.getAvgScore(memberId);
	}

	@Override
	public Score getHighestScore(UUID memberId) {
		return scoreboardService.getHighestScore(memberId);
	}
}
