package cz.elder.scrabble.scrabble.controller;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cz.elder.scrabble.scrabble.domain.Member;

@RequestMapping("/member")
public interface MemberOperations {

	@GetMapping("/all")
	List<Member> getAllMembers();

	@GetMapping("")
	Optional<Member> getMemberByEmail(@RequestParam String email);

	@PostMapping("")
	void registerNewMember(@Valid @RequestBody Member member);

	@PutMapping("")
	void updateMember(@Valid @RequestBody Member member);
}
