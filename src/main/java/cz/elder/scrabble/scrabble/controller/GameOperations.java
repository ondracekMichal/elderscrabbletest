package cz.elder.scrabble.scrabble.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cz.elder.scrabble.scrabble.domain.Game;
import cz.elder.scrabble.scrabble.service.dto.GameOpponent;
import cz.elder.scrabble.scrabble.service.dto.GameScoreDTO;

@RequestMapping("/game")
public interface GameOperations {

	@GetMapping("")
	List<Game> getAllGames();

	@GetMapping("/{gameId}")
	Game getGame(@PathVariable UUID gameId);

	@PostMapping("")
	void saveNewGame(@Valid @RequestBody GameScoreDTO gameScore);

	@GetMapping("/opponent")
	GameOpponent getOpponentForGame(@RequestParam(name = "game") UUID gameId, @RequestParam(name = "member") UUID memberId);
}
