package cz.elder.scrabble.scrabble.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;

import cz.elder.scrabble.scrabble.domain.Game;
import cz.elder.scrabble.scrabble.service.GameService;
import cz.elder.scrabble.scrabble.service.dto.GameOpponent;
import cz.elder.scrabble.scrabble.service.dto.GameScoreDTO;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class GameController implements GameOperations {

	private GameService gameService;

	@Override
	public List<Game> getAllGames() {
		return null;
	}

	@Override
	public Game getGame(UUID gameId) {
		return null;
	}

	@Override
	public void saveNewGame(@Valid GameScoreDTO gameScore) {
		if (gameScore.getWinnerScore() <= gameScore.getLoserScore())
			throw new IllegalArgumentException("Winner did not achieve more points than loser");

		if (gameScore.getLoserMemberId().equals(gameScore.getWinnerMemberId()) )
			throw new IllegalArgumentException("Winner and loser have to be different members");

		gameService.saveNewGame(gameScore);
	}

	@Override
	public GameOpponent getOpponentForGame(UUID gameId, UUID memberId) {
		return gameService.getOpponentForGame(gameId, memberId);
	}
}
