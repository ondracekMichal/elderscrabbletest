package cz.elder.scrabble.scrabble.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cz.elder.scrabble.scrabble.domain.Score;
import cz.elder.scrabble.scrabble.domain.views.ScoreboardView;


@RequestMapping("/scoreboard")
public interface ScoreboardOperations {

	@GetMapping("")
	List<ScoreboardView> getScoreboard();

	@GetMapping("/highest")
	Score getHighestScore();

	@GetMapping("/lowest")
	Score getLowestScore();

	@GetMapping("/{memberId}/wins")
	int getMemberWins(@PathVariable UUID memberId);

	@GetMapping("/{memberId}/loss")
	int getMemberLoss(@PathVariable UUID memberId);

	@GetMapping("/{memberId}/avg-score")
	Double getAvgScore(@PathVariable UUID memberId);

	@GetMapping("/{memberId}/highest-score")
	Score getHighestScore(@PathVariable UUID memberId);
}
