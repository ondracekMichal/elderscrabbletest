package cz.elder.scrabble.scrabble.domain;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Member {

	@Id
	@GeneratedValue
	@Column(name = "member_id")
	private UUID memberId;

	@NotBlank
	private String name;

	@Email
	private String email;

	@NotNull
	@Column(name = "phone_number")
	@Pattern(regexp="^(\\+420|\\+421)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}")
	private String phoneNumber;

	@Column(name = "registration_date", insertable=false)
	private LocalDateTime registrationDate;

	@OneToMany(mappedBy = "member", cascade = CascadeType.MERGE)
	@JsonIgnore
	private Set<Score> scores = new HashSet<>();
}
