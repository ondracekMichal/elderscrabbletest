package cz.elder.scrabble.scrabble.domain;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.NamedQuery;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@NamedQuery(
		name = "gameOpponent",
		query = "SELECT s.memberId from Score s WHERE  s.gameId  = :gameId and s.memberId <> :memberId",
		fetchSize = 1
)
public class Game {


	@Id
	@GeneratedValue
	@Column(name = "game_id")
	private UUID gameId;

	@Column(name = "played_at", insertable=false)
	private LocalDateTime playedAt;

	@OneToMany(mappedBy = "game", cascade = CascadeType.MERGE)
	private Set<Score> scores = new HashSet<>();
}
