package cz.elder.scrabble.scrabble.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cz.elder.scrabble.scrabble.domain.idClass.ScoreId;
import lombok.Data;

@Data
@Entity
@IdClass(ScoreId.class)
public class Score {

	@Id
	@Column(name = "member_id")
	private UUID memberId;

	@Id
	@Column(name = "game_id")
	private UUID gameId;

	@Range(min = 0, max = 1000)
	private int score;

	@NotNull
	private boolean winner;

	@Id
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "member_id", insertable = false, updatable = false)
	@JsonIgnore
	private Member member;

	@Id
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "game_Id", insertable = false, updatable = false)
	@JsonIgnore
	private Game game;
}
