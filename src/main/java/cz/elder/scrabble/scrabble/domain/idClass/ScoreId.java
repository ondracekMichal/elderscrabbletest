package cz.elder.scrabble.scrabble.domain.idClass;

import java.io.Serializable;
import java.util.UUID;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScoreId implements Serializable {

	private UUID memberId;

	private UUID gameId;
}
