package cz.elder.scrabble.scrabble.domain.views;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Immutable
@Entity
public class ScoreboardView {

	@Id
	private String name;

	@Column(name = "average_score", columnDefinition = "NUMERIC")
	private Double averageScore;
}
