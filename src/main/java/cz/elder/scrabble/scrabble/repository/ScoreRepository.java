package cz.elder.scrabble.scrabble.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cz.elder.scrabble.scrabble.domain.Score;

public interface ScoreRepository extends JpaRepository<Score, UUID> {

	int countByMemberIdAndWinner(UUID memberId, boolean isWinner);

	@Query(value = "SELECT AVG(s.score) from Score s where s.memberId = ?1")
	Double getAverageScore(UUID memberId);

	Score findTop1ByMemberIdOrderByScoreDesc(UUID memberId);

	Score findTopByScoreIsNotNullOrderByScore();

	Score findTopByScoreIsNotNullOrderByScoreDesc();

}
