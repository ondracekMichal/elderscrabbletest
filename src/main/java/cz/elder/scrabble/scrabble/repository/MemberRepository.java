package cz.elder.scrabble.scrabble.repository;


import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cz.elder.scrabble.scrabble.domain.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, UUID> {

	Optional<Member> findMemberByEmail(String email);

	Optional<Member> findByMemberId(UUID memberId);
}
