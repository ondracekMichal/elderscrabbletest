package cz.elder.scrabble.scrabble.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.elder.scrabble.scrabble.domain.views.ScoreboardView;

public interface ScoreBoardViewRepository extends JpaRepository<ScoreboardView, String> {
}
