package cz.elder.scrabble.scrabble.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cz.elder.scrabble.scrabble.domain.Game;
import cz.elder.scrabble.scrabble.service.dto.GameOpponent;

public interface GameRepository extends JpaRepository<Game, UUID> {

	@Query(value = "SELECT new cz.elder.scrabble.scrabble.service.dto.GameOpponent(s.memberId, s.score, game.playedAt) "
			+ "from Score s join s.game as game WHERE  s.gameId  = ?1 and s.memberId <> ?2")
	GameOpponent getGameOpponent(UUID gameId, UUID memberId);

	Game getGameByGameId(UUID gameId);
}
