package cz.elder.scrabble.scrabble.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import cz.elder.scrabble.scrabble.domain.Member;
import cz.elder.scrabble.scrabble.repository.MemberRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class MemberServiceImpl implements MemberService {

	private MemberRepository memberRepository;

	@Override
	public List<Member> getAllMembers() {
		return memberRepository.findAll();
	}

	@Override
	public Optional<Member> getMemberByEmail(String email) {
		return memberRepository.findMemberByEmail(email);
	}

	@Override
	public Optional<Member> getMemberByMemberId(UUID memberId) {
		return memberRepository.findByMemberId(memberId);
	}

	@Override
	public Member registerNewMember(Member member) {
		return memberRepository.save(member);
	}

	@Override
	public void updateMember(Member member) {
		member.setRegistrationDate(LocalDateTime.now());
		memberRepository.save(member);

	}
}
