package cz.elder.scrabble.scrabble.service;

import java.util.List;
import java.util.UUID;

import cz.elder.scrabble.scrabble.domain.Score;
import cz.elder.scrabble.scrabble.domain.views.ScoreboardView;


public interface ScoreboardService {

	List<ScoreboardView> getScoreboard();

	Score getHighestScore();

	Score getLowestScore();

	int getMemberWins(UUID memberId);

	int getMemberLoss(UUID memberId);

	Double getAvgScore(UUID memberId);

	Score getHighestScore(UUID memberId);
}
