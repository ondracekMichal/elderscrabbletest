package cz.elder.scrabble.scrabble.service.dto;

import java.time.LocalDateTime;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameOpponent {

	private UUID opponentMemberId;

	private int opponentScore;

	private LocalDateTime payedAt;
}
