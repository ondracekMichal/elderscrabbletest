package cz.elder.scrabble.scrabble.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import cz.elder.scrabble.scrabble.domain.Member;


public interface MemberService {

	List<Member> getAllMembers();

	Optional<Member> getMemberByEmail(String email);

	Optional<Member> getMemberByMemberId(UUID memberId);

	Member registerNewMember(Member member);

	void updateMember(Member member);
}
