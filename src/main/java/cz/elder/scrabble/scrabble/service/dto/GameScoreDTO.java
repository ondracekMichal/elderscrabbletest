package cz.elder.scrabble.scrabble.service.dto;

import java.util.UUID;

import lombok.Data;

@Data
public class GameScoreDTO {

	private UUID winnerMemberId;

	private int winnerScore;

	private UUID loserMemberId;

	private int loserScore;
}
