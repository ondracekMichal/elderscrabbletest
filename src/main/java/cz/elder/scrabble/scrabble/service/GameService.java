package cz.elder.scrabble.scrabble.service;

import java.util.List;
import java.util.UUID;

import cz.elder.scrabble.scrabble.domain.Game;
import cz.elder.scrabble.scrabble.service.dto.GameOpponent;
import cz.elder.scrabble.scrabble.service.dto.GameScoreDTO;

public interface GameService {

	List<GameScoreDTO> getAllGames();

	Game saveNewGame(GameScoreDTO gameScore);

	GameOpponent getOpponentForGame(UUID gameId, UUID memberId);

	Game getGame(UUID gameId);
}
