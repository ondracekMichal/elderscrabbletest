package cz.elder.scrabble.scrabble.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import cz.elder.scrabble.scrabble.domain.Score;
import cz.elder.scrabble.scrabble.domain.views.ScoreboardView;
import cz.elder.scrabble.scrabble.repository.ScoreBoardViewRepository;
import cz.elder.scrabble.scrabble.repository.ScoreRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class ScoreboardServiceImpl implements ScoreboardService {

	private ScoreBoardViewRepository scoreBoardViewRepository;

	private ScoreRepository scoreRepository;

	@Override
	public List<ScoreboardView> getScoreboard() {
		return scoreBoardViewRepository.findAll();
	}

	@Override
	public Score getHighestScore() {
		return scoreRepository.findTopByScoreIsNotNullOrderByScoreDesc();
	}

	@Override
	public Score getLowestScore() {
		return scoreRepository.findTopByScoreIsNotNullOrderByScore();
	}

	@Override
	public int getMemberWins(UUID memberId) {
		return scoreRepository.countByMemberIdAndWinner(memberId, true);
	}

	@Override
	public int getMemberLoss(UUID memberId) {
		return scoreRepository.countByMemberIdAndWinner(memberId, false);
	}

	@Override
	public Double getAvgScore(UUID memberId) {
		return scoreRepository.getAverageScore(memberId);
	}

	@Override
	public Score getHighestScore(UUID memberId) {
		return scoreRepository.findTop1ByMemberIdOrderByScoreDesc(memberId);
	}
}
