package cz.elder.scrabble.scrabble.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.elder.scrabble.scrabble.domain.Game;
import cz.elder.scrabble.scrabble.domain.Score;
import cz.elder.scrabble.scrabble.repository.GameRepository;
import cz.elder.scrabble.scrabble.repository.ScoreRepository;
import cz.elder.scrabble.scrabble.service.dto.GameOpponent;
import cz.elder.scrabble.scrabble.service.dto.GameScoreDTO;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class GameServiceImpl implements GameService {

	private GameRepository gameRepository;

	private ScoreRepository scoreRepository;

	@Override
	public List<GameScoreDTO> getAllGames() {
		return null;
	}

	@Override
	@Transactional
	public Game saveNewGame(GameScoreDTO gameScore) {

		final Game newGame = gameRepository.save(new Game());

		scoreRepository.save(createScore(newGame.getGameId(), gameScore.getWinnerMemberId(), gameScore.getWinnerScore(), true));
		scoreRepository.save(createScore(newGame.getGameId(), gameScore.getLoserMemberId(), gameScore.getLoserScore(), false));

		return newGame;
	}

	@Override
	public GameOpponent getOpponentForGame(UUID gameId, UUID memberId) {
		return gameRepository.getGameOpponent(gameId, memberId);
	}

	@Override
	public Game getGame(UUID gameId) {
		return gameRepository.getGameByGameId(gameId);
	}

	private Score createScore(UUID gameId, UUID memberId, int score, boolean isWinner) {
		final Score memberScore = new Score();

		memberScore.setGameId(gameId);
		memberScore.setMemberId(memberId);

		memberScore.setScore(score);
		memberScore.setWinner(isWinner);

		return memberScore;
	}
}
